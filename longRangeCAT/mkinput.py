import sys
import numpy as np
sys.path.insert(0, '..')
from inputparams import writeInput




params={}

# ~ dfile="g0d61-e0d63-h0d41"
# ~ params['potential']="ModulatedPendulum"
# ~ params['h']=0.41
# ~ params['gamma']=0.61
# ~ params['epsilon']=0.63
# ~ params['phi']=np.pi
# ~ params['Npcell']=32

# ~ dfile="g0d45-e0d40-h0d35"
# ~ params['potential']="ModulatedPendulum"
# ~ params['h']=0.35
# ~ params['gamma']=0.45
# ~ params['epsilon']=0.40
# ~ params['phi']=0
# ~ params['Npcell']=32

dfile="g0d61-e0d63-h0d41-sigma15-nuC4d5-tmax2500"
params['potential']="ModulatedPendulum"
params['dirspectra']="/tmpdir/p0110mm/longRangeCAT/spectrumBeta2-g0d61-e0d63-h0d41-547960"
params['sigma']=15
params['tmax']=2500
params['tcheck']=25
params['nuC']=4.5
params['beta0']=0

# ~ dfile="test2"
# ~ params['potential']="ModulatedPendulum"
# ~ params['h']=0.346
# ~ params['gamma']=0.374
# ~ params['epsilon']=0.244
# ~ params['phi']=0.0
# ~ params['Npcell']=32
# ~ params['x0']=1.72

# ~ dfile="2ilots-g0d225-e0d59-h0d346"
# ~ params['potential']="ModulatedPendulum-2ilots"
# ~ params['h']=0.346
# ~ params['gamma']=0.374
# ~ params['epsilon']=0.244
# ~ params['phi']=0.0
# ~ params['Npcell']=32



# ~ dfile="g0d20-e0d15-h0d40-tmax25000"
# ~ params['potential']="ModulatedPendulum"
# ~ params['dirspectrum']="/tmpdir/p0110mm/longRangeCAT/spectrumBeta-g0d20-e0d15-h0d4"
# ~ params['tmax']=25000
# ~ params['tcheck']=50


# ~ dfile="sigma25-N1079-Fem6em4-g0d20-e0d15-h0d4"
# ~ params['potential']="ModulatedPendulum"
# ~ params['dirspectrumchaotic']="/tmpdir/p0110mm/longRangeCAT/spectrumBeta-g0d20-e0d15-h0d4-Npcell32-400994"
# ~ params['dirspectrumregular']="/tmpdir/p0110mm/longRangeCAT/spectrumBeta-g0d20-e0d0-h0d4-Npcell32-400993"
# ~ params['sigma']=25
# ~ params['Fmin']=10**(-6)
# ~ params['Fmax']=10**(-4)


# ~ params['potential']="KickedRotor"
# ~ params['Npcell']=16
# ~ params['K']=3.923
# ~ params['K']=4.00
# ~ params['h']=0.12

# ~ params['potential']="KickedRotor-alpha"
# ~ params['Npcell']=32
# ~ params['K']=4

writeInput("inputs/"+dfile+".txt",params)


