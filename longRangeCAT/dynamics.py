import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns-TB")
	os.mkdir(wdir+"figs-TB")
	os.mkdir(wdir+"dataruns-real")
	os.mkdir(wdir+"figs-real")
	
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	dirspectrumchaotic=params['dirspectrumchaotic']
	dirspectrumregular=params['dirspectrumregular']
	
	params=readInput(dirspectrumchaotic+"/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['nruns'])
	addParams(wdir+"params.txt",{'epsilon':e})
	addParams(wdir+"params.txt",{'gamma':gamma})
	addParams(wdir+"params.txt",{'Npcell':Npcell})
	addParams(wdir+"params.txt",{'Ncell':Ncell})
	addParams(wdir+"params.txt",{'h':h})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	dirspectrumchaotic=params['dirspectrumchaotic']
	dirspectrumregular=params['dirspectrumregular']
	sigma=float(params['sigma'])
	Fbmin=float(params['Fmin'])
	Fbmax=float(params['Fmax'])
	nruns=int(params['nruns'])
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['Ncell'])
	
	Fb=np.append(np.array([0]),np.logspace(np.log10(Fbmin),np.log10(Fbmax),nruns-1))[runid]
	
	if runid==0:
		tmax=int(0.5*h//Fbmin)
		ncheck=min(int(0.5*h//Fbmin),500)
	else:
		tmax=int(0.5*h//Fb)
		ncheck=min(int(0.5*h//Fb),500)
	
	tcheck=int(tmax//ncheck)+1
	
	ncheck=int(tmax//tcheck)+1
	
	# 1. dyn TB
	
	grid=Grid(Ncell,h,xmax=Ncell)
	ham=Hamiltonian(grid,spectrumfile=dirspectrumchaotic
		+"/data.npz")+Hamiltonian(grid,M=np.diag(Fb*grid.x))
	
	
	if runid==0:
		# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
		i0=int((Ncell-1)/2)
		wf=WaveFunction(grid)
		wf.setState('diracx',i0=i0)
	else:
		wf=WaveFunction(grid)
		wf.x=np.exp(-(grid.x)**2/(2*sigma**2))
		wf.normalize("x")
	
	tp=TimePropagator(grid,ham,dt=4*np.pi)

	prob=np.zeros((ncheck,Ncell))
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	time=np.zeros(ncheck)

	for it in range(tmax):
		wf=tp%wf # Propagation

		if it%tcheck==0:
			prob[int(it/tcheck)]=np.abs(wf.x)**2
			prob[int(it/tcheck)]/=np.max(prob[int(it/tcheck)])
			xm[int(it/tcheck)]=np.sum(grid.x*np.abs(wf.x)**2)
			xstd[int(it/tcheck)]=np.sqrt(np.sum((grid.x-xm[int(it/tcheck)])**2*np.abs(wf.x)**2))
			time[int(it/tcheck)]=it
			# ~ print(it,tcheck,int(it/tcheck),int(tmax/tcheck))
			# ~ print(int(it/tcheck))
			
	prob[-1]=prob[-2]
	time[-1]=time[-2]
	xm[-1]=xm[-2]
	xstd[-1]=xstd[-2]
	
	np.savez(wdir+"dataruns-TB/"+str(runid),prob=prob,time=time,xm=xm,Fb=Fb,xstd=xstd)	
		
	ax=plt.gca()
	plt.clf()
	ax.set_title("F="+str(Fb))
	ax=plt.subplot(2,1,1)
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(prob,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(2,1,2)
	ax.plot(time,xm)
	ax.set_xlim(np.min(time),np.max(time))
	plt.savefig(wdir+"figs-TB/"+str(runid)+".png",dpi=50)
	
	
	
	# 2. dyn real
	
	data=np.load(dirspectrumregular+"/data.npz")
	wannierx=data['wannierx']
	data.close()
	
	grid=Grid(Npcell*Ncell,h,Ncell*2*np.pi)
	pot=ModulatedPendulum(e,gamma,Fb=Fb)
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	wf0=WaveFunction(grid)
	wf0.x=np.abs(wannierx)
	wf0.normalize("x")
	
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf0.x,(i-int(0.5*(Ncell-1)))*Npcell)
		
	if runid==0:
		# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
		i0=int((Ncell-1)/2)
		wf=WaveFunction(grid)
		wf.x=wf0.x
		wf.normalize("x")
	else:
		wf=WaveFunction(grid)
		for i in range(Ncell):
			wf.x+=wfcell[i].x*np.exp(-(grid.x)**2/(2*(sigma*2*np.pi)**2))
		wf.normalize("x")
	
	prob=np.zeros((ncheck,Ncell))
	probReal=np.zeros((ncheck,Ncell))
	probReg=np.zeros(ncheck)
	
	xm=np.zeros(ncheck)
	xstd=np.zeros(ncheck)
	time=np.zeros(ncheck)
	
	for it in range(tmax):
		wf=floquet%wf
		
		if it%tcheck==0:	
			prob[int(it/tcheck)]=np.array([wf//wfi for wfi in wfcell])
			probReal[int(it/tcheck)]=np.array([np.sum(np.abs(wf.x[i*Npcell:i*Npcell+Npcell])**2) for i in range(Ncell)])
			
			probReg[int(it/tcheck)]=np.sum(prob[int(it/tcheck)])
			
			prob[int(it/tcheck)]/=np.max(prob[int(it/tcheck)])
			probReal[int(it/tcheck)]/=np.max(probReal[int(it/tcheck)])

			xm[int(it/tcheck)]=np.sum(grid.x*np.abs(wf.x)**2/(2*np.pi))
			xstd[int(it/tcheck)]=np.sqrt(np.sum((grid.x/(2*np.pi)-xm[int(it/tcheck)])**2*np.abs(wf.x)**2))
			time[int(it/tcheck)]=it	
			
	prob[-1]=prob[-2]
	probReal[-1]=probReal[-2]
	probReg[-1]=probReg[-2]
	time[-1]=time[-2]
	xm[-1]=xm[-2]
	xstd[-1]=xstd[-2]
	np.savez(wdir+"dataruns-real/"+str(runid),prob=prob,time=time,xm=xm,probReg=probReg,Fb=Fb,xstd=xstd,probReal=probReal)
			
	ax=plt.gca()
	ax.set_title("F="+str(Fb))
	plt.clf()
	ax=plt.subplot(3,1,1)
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(probReal,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(3,1,2)
	ax.plot(time,xm)
	ax.set_xlim(np.min(time),np.max(time))
	
	ax=plt.subplot(3,1,3)
	ax.plot(time,probReg)
	ax.set_xlim(np.min(time),np.max(time))

	
	plt.savefig(wdir+"figs-real/"+str(runid)+".png",dpi=50)

	
	
# ~ if mode=="gather":

	
	# ~ os.system("rm -r "+wdir+"dataruns/")
	
# ~ if mode=="plot":
	
	# ~ params=readInput(wdir+"params.txt")
	# ~ dirspectrumchaotic=params['dirspectrumchaotic']
	# ~ dirspectrumregular=params['dirspectrumregular']
	# ~ sigma=float(params['sigma'])
	# ~ Fbmin=float(params['Fmin'])
	# ~ Fbmax=float(params['Fmax'])
	# ~ nruns=int(params['nruns'])
	# ~ e=float(params['epsilon'])
	# ~ gamma=float(params['gamma'])
	# ~ h=float(params['h'])
	# ~ Npcell=int(params['Npcell'])
	# ~ Ncell=int(params['Ncell'])
	
	# ~ for i in range(nruns)
		# ~ data=np.load(wdir+"dataruns-real/"+str(runid)
		# ~ quasienergies=data['quasienergies']
		# ~ data.close()
		
		# ~ data=np.load(wdir+"dataruns-TB/"+str(runid)
		# ~ quasienergies=data['quasienergies']
		# ~ data.close()
	

	

	# ~ plt.savefig(wdir+"spectrum2.png", bbox_inches='tight',dpi=250)	
	
