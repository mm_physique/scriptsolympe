import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns-TB")
	os.mkdir(wdir+"figs-TB")
	os.mkdir(wdir+"dataruns-real")
	os.mkdir(wdir+"figs-real")
	
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	dirspectrum=params['dirspectrum']
	
	params=readInput(dirspectrum+"-N1079/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['nruns'])
	addParams(wdir+"params.txt",{'epsilon':e})
	addParams(wdir+"params.txt",{'gamma':gamma})
	addParams(wdir+"params.txt",{'Npcell':Npcell})
	addParams(wdir+"params.txt",{'Ncell':Ncell})
	addParams(wdir+"params.txt",{'h':h})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	dirspectrum=params['dirspectrum']
	nruns=int(params['nruns'])
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['Ncell'])
	tmax=int(params['tmax'])
		
	dirspectrum=dirspectrum+"-N"+str(Ncell)
	
	alpha=np.logspace(-2,2,nruns)[runid]
	
	
	# 1. dyn TB
	grid=Grid(Ncell,h,xmax=Ncell)
	ham=Hamiltonian(grid,spectrumfile=dirspectrum+"/data-chaotic.npz")
	# ~ e0=np.real(ham.Vn[0])
	V1=np.abs(ham.Vn[1])/2
	elocal=alpha*V1*(np.random.rand(Ncell)-0.5)
	ham=Hamiltonian(grid,spectrumfile=dirspectrum+"/data-chaotic.npz")+Hamiltonian(grid,M=np.diag(elocal))
	
	if runid==0:
		# ~ addParams(wdir+"params.txt",{'e0':e0})
		addParams(wdir+"params.txt",{'V1':V1})
		
	# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
	i0=int((Ncell-1)/2)
	wf=WaveFunction(grid)
	wf.setState('diracx',i0=i0)
		
	tp=TimePropagator(grid,ham,dt=4*np.pi)

	prob=np.zeros((tmax,Ncell))
	xm=np.zeros(tmax)
	xabsm=np.zeros(tmax)
	xstd=np.zeros(tmax)
	xabsstd=np.zeros(tmax)
	time=np.zeros(tmax)

	for it in range(tmax):
		wf=tp%wf # Propagation


		prob[it]=np.abs(wf.x)**2
		prob[it]/=np.max(prob[it])
		xm[it]=np.sum(grid.x*np.abs(wf.x)**2)
		xstd[it]=np.sqrt(np.sum((grid.x-xm[it])**2*np.abs(wf.x)**2))
		xabsm[it]=np.sum(np.abs(grid.x)*np.abs(wf.x)**2)
		xabsstd[it]=np.sqrt(np.sum((np.abs(grid.x)-xabsm[it])**2*np.abs(wf.x)**2))
		time[it]=it

	
	np.savez(wdir+"dataruns-TB/"+str(runid),
		prob=prob,
		time=time,
		xm=xm,
		xstd=xstd,
		xabsm=xabsm,
		xabsstd=xabsstd,
		alpha=alpha)	
		
	ax=plt.gca()
	plt.clf()
	ax=plt.subplot(2,1,1)
	ax.set_title(str(alpha))
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(prob,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(2,1,2)
	ax.plot(time,xstd)
	ax.set_xlim(np.min(time),np.max(time))
	plt.savefig(wdir+"figs-TB/"+str(runid)+".png",dpi=50)
	
	
	
	# 2. dyn real
	
	data=np.load(dirspectrum+"/data-reg.npz")
	wannierx=data['wannierx']
	data.close()
	
	grid=Grid(Npcell*Ncell,h,Ncell*2*np.pi)
	
	pot=ModulatedPendulumDisorder(e,gamma,np.repeat(elocal,Npcell))
		
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	wf0=WaveFunction(grid)
	wf0.x=np.abs(wannierx)
	wf0.normalize("x")
	
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf0.x,(i-int(0.5*(Ncell-1)))*Npcell)
		

	# ~ # N is odd eg: 5 => i = 0,1,[2],3,4 and (5-1)/2 =2
	i0=int((Ncell-1)/2)
	wf=WaveFunction(grid)
	wf.x=wf0.x
	wf.normalize("x")
	
	prob=np.zeros((tmax,Ncell))
	probReal=np.zeros((tmax,Ncell))
	probReg=np.zeros(tmax)
	
	xm=np.zeros(tmax)
	xstd=np.zeros(tmax)
	xabsm=np.zeros(tmax)
	xabsstd=np.zeros(tmax)
	
	time=np.zeros(tmax)
	xmloc=np.zeros(tmax)
	xstdloc=np.zeros(tmax)
	xabsmloc=np.zeros(tmax)
	xabsstdloc=np.zeros(tmax)
	
	for it in range(tmax):
		wf=floquet%wf
		
	
		prob[it]=np.array([wf//wfi for wfi in wfcell])
		probReal[it]=np.array([np.sum(np.abs(wf.x[i*Npcell:i*Npcell+Npcell])**2) for i in range(Ncell)])
		
		probReg[it]=np.sum(prob[it])
		
		xm[it]=np.sum(grid.x*np.abs(wf.x)**2/(2*np.pi))
		xstd[it]=np.sqrt(np.sum((grid.x/(2*np.pi)-xm[it])**2*np.abs(wf.x)**2))
		xabsm[it]=np.sum(np.abs(grid.x)*np.abs(wf.x)**2)
		xabsstd[it]=np.sqrt(np.sum((np.abs(grid.x)-xabsm[it])**2*np.abs(wf.x)**2))
		
		
		
		gridn=np.arange(Ncell)-(Ncell-1)/2
		xmloc[it]=np.sum(gridn*prob[it])
		xstdloc[it]=np.sqrt(np.sum((gridn-xmloc[it])**2*prob[it]))
		xabsmloc[it]=np.sum(np.abs(gridn)*prob[it])
		xabsstdloc[it]=np.sqrt(np.sum((np.abs(gridn)-xabsmloc[it])**2*prob[it]))
		time[it]=it	
		
		
		# Renormalisation pour meilleur contraste
		prob[it]/=np.max(prob[it])
		probReal[it]/=np.max(probReal[it])

	np.savez(wdir+"dataruns-real/"+str(runid),
		prob=prob,
		time=time,
		xm=xm,
		xstd=xstd,
		xabsm=xabsm,
		xabsstd=xabsstd,
		xmloc=xmloc,
		xstdloc=xstdloc,
		xabsmloc=xabsmloc,
		xabsstdloc=xabsstdloc,
		probReg=probReg,
		probReal=probReal,
		alpha=alpha)
			
	ax=plt.gca()
	plt.clf()
	ax=plt.subplot(3,1,1)
	ax.set_title(str(alpha))
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	prob=np.swapaxes(probReal,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	
	im=ax.contourf(times,n,prob, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax=plt.subplot(3,1,2)
	ax.plot(time,xm)
	ax.set_xlim(np.min(time),np.max(time))
	
	ax=plt.subplot(3,1,3)
	ax.plot(time,probReg)
	ax.set_xlim(np.min(time),np.max(time))

	
	plt.savefig(wdir+"figs-real/"+str(runid)+".png",dpi=50)

	
	
# ~ if mode=="gather":

	
	# ~ os.system("rm -r "+wdir+"dataruns/")
	
# ~ if mode=="plot":
	
	# ~ params=readInput(wdir+"params.txt")
	# ~ dirspectrumchaotic=params['dirspectrumchaotic']
	# ~ dirspectrumregular=params['dirspectrumregular']
	# ~ sigma=float(params['sigma'])
	# ~ Fbmin=float(params['Fmin'])
	# ~ Fbmax=float(params['Fmax'])
	# ~ nruns=int(params['nruns'])
	# ~ e=float(params['epsilon'])
	# ~ gamma=float(params['gamma'])
	# ~ h=float(params['h'])
	# ~ Npcell=int(params['Npcell'])
	# ~ Ncell=int(params['Ncell'])
	
	# ~ for i in range(nruns)
		# ~ data=np.load(wdir+"dataruns-real/"+str(runid)
		# ~ quasienergies=data['quasienergies']
		# ~ data.close()
		
		# ~ data=np.load(wdir+"dataruns-TB/"+str(runid)
		# ~ quasienergies=data['quasienergies']
		# ~ data.close()
	

	

	# ~ plt.savefig(wdir+"spectrum2.png", bbox_inches='tight',dpi=250)	
	
