import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	h=2*np.pi/Npcell
	addParams(wdir+"params.txt",{'h':h})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	K=float(params['K'])

	beta0=np.linspace(0,1,nruns,endpoint=False)
	beta0=np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))

	h=2*np.pi/Npcell
	pot=KickedRotor(K)
	grid=Grid(Npcell,h)
	
	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	
	overlaps=np.zeros((nruns,Npcell),dtype=complex)
	quasienergies=np.zeros((nruns,Npcell))
	evec0p=np.zeros((nruns,Npcell),dtype=complex)
	
	for i in range(nruns):
		floquet=FloquetTimePropagator(grid,pot,alpha=beta0[i],beta=beta0[runid],T0=1,idtmax=1)
		floquet.diagonalize()
	
		overlaps[i]=floquet.orderEigenstatesWithOverlapOn(wf)
		quasienergies[i]=floquet.quasienergy
		evec0p[i]=floquet.eigenvec[0].p
	
	np.savez(wdir+"dataruns/"+str(runid),"w", quasienergies=quasienergies, overlaps=overlaps,evec0p=evec0p)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	h=float(params['h'])
	
	quasienergies=np.zeros((nruns,nruns,Npcell))
	overlaps=np.zeros((nruns,nruns,Npcell),dtype=complex)
	beta=np.zeros((nruns,nruns,Npcell))
	
	
	beta0=np.linspace(0,1,nruns,endpoint=False)
	beta0=np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))
	
	Ncell=nruns
	
	grid=Grid(Ncell*Npcell,h,xmax=Ncell*2*np.pi)
	wannier=WaveFunction(grid)
	# ~ ind=np.flipud(np.arange(0,Ncell*Npcell,Ncell))
	
	for ibeta in range(nruns):
			# ~ beta[irun]=np.ones(Npcell)*np.sort(beta0*(beta0<=0.5)+(beta0-1)*(beta0>0.5))[irun]
			
		data=np.load(wdir+"dataruns/"+str(ibeta)+".npz")
		quasienergies[ibeta]=data['quasienergies']
		overlaps[ibeta]=data['overlaps']
		# ~ wannier.p[ind]=np.abs(data['evec0p'])
		data.close()

		# ~ ind=ind+1

	# ~ wannier.p=np.roll(wannier.p,int(Ncell/2+1))
	# ~ wannier.p=np.fft.fftshift(wannier.p*grid.phaseshift)

	# ~ wannier.normalize("p")


	np.savez(wdir+"data","w", quasienergies=quasienergies, overlaps=overlaps,beta=beta,wannierx=np.abs(wannier.x),gridx=grid.x)	
	
	# ~ os.system("rm -r "+wdir+"dataruns/")
	
if mode=="plot":
	
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	
	data=np.load(wdir+"data.npz")
	quasienergies=data['quasienergies']
	overlaps=data['overlaps']
	# ~ beta=data['beta']
	# ~ gridx=data['gridx']
	# ~ wannierx=data['wannierx']
	
	ax=plt.subplot(1,2,1)
	ax.imshow(quasienergies[:,:,0],extent=[-0.5,0.5,-0.5,0.5],cmap=cm.gnuplot2)
	ax.set_xlabel(r"Quasi-impulsion")
	ax.set_ylabel(r"Quasi-position")
	
	ax=plt.subplot(1,2,2)
	
	Vn=np.fft.fftn(quasienergies[:,:,0])
	
	n0=int((nruns-1)*0.5)
	Vn[0,0]=Vn[1,1]
	Vn=np.roll(Vn,n0,0)
	Vn=np.roll(Vn,n0,1)
	
	ax.set_ylim(0,n0)
	ax.set_xlim(0,n0)
	ax.set_xlabel(r"Distance en site de position")
	ax.set_ylabel(r"Distance en site d'impulsion")
	
	
	ax.imshow(np.log(np.abs(Vn)),extent=[-n0,n0,-n0,n0],cmap=cm.gnuplot2)
	
	plt.savefig(wdir+"Vn.png", bbox_inches='tight',dpi=250)
	
	
