import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	nav=int(params['nav'])
	alpha=float(params['alpha'])
	i0=int(params['i0'])
	nruns=int(params['nruns'])
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	
	ncheck=int(tmax/tcheck)+1
	
	grid=Grid(N,h=1,xmax=2*np.pi)
	
	if potential=="Rectangle":
		pot=Rectangle(alpha)
		
	if potential=="RectangleAsym":
		pot=Rectangle(alpha,x1=0,x2=np.pi)
	
	if potential=="SawTooth":
		pot=SawTooth(alpha)	
	
	lambdacfs=np.zeros((ncheck,N))
	
	

	for n in range(nav):
		fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
		wf=WaveFunction(grid)
		wf.setState("diracx",i0=i0)
		icheck=0
		for it in range(0,tmax-1): #pas trés propre pour le premier tic
			wf=fo%wf
			
			lambdacfs[icheck]+=np.abs(wf.x)**2	
			
			if it%tcheck==0:
				icheck+=1
			

	np.savez(wdir+"dataruns/"+str(runid),lambdacfs=lambdacfs/(nav*tcheck))
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	nruns=int(params['nruns'])
	N=int(params['N'])
	alpha=float(params['alpha'])
	nav=int(params['nav'])
	tmax=int(params['tmax'])
	tcheck=int(params['tcheck'])
	
	ncheck=int(tmax/tcheck)+1
	
	lambdacfs=np.zeros((ncheck,N))
	
	for i in range(nruns):
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		lambdacfs+=data['lambdacfs']
		data.close()
		
	lambdacfs/=nruns
	
	time=1+np.arange(ncheck)*tcheck
	
	np.savez(wdir+"data",time=time,lambdacfs=lambdacfs*N-1)
	os.system("rm -r "+wdir+"dataruns/")

				
	


