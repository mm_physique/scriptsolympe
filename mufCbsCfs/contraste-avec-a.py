import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")

	inputfile=sys.argv[3]+".txt"
	os.system("cp "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})

if mode=="compute":
	# Loading input file
	
	runid=int(sys.argv[3])-1
	params=readInput(wdir+"params.txt")
	potential=params['potential']
	N=int(params['N'])
	nav=int(params['nav'])
	i0=int(params['i0'])
	nruns=int(params['nruns'])	
	
	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]
	ialpha=runid%nalpha
	alpha=alphat[ialpha]
		
	lambdainf=np.zeros(nav)
	ipr=np.zeros(nav)
	
	for iav in range(nav):
		grid=Grid(N,h=1,xmax=2*np.pi)
		pot=SawTooth(alpha)
		
		fo=FloquetTimePropagator(grid,pot,T0=1,idtmax=1,randomphase=True)
		fo.diagonalize()
		
		ipr[iav]=np.sum(np.fromiter((np.sum(np.abs(fo.eigenvec[ievec].p)**4) for ievec in range(N)),dtype=float))
		lambdainf[iav]=np.sum(np.fromiter((np.abs(fo.eigenvec[ievec].x[i0])**4 for ievec in range(N)),dtype=float))
		
			
			# ~ ipr[ialpha,iav]=np.sum(np.array([np.sum(np.abs(fo.eigenvec[ievec].p)**4) for ievec in range(N)]))
			# ~ lambdainf[ialpha,iav]=N*np.sum(np.array([np.sum(np.abs(fo.eigenvec[ievec].x[i0])**4) for ievec in range(N)]))
			
			# ~ ipr[ialpha,iav]=np.sum(np.fromiter((np.sum(np.abs(fo.eigenvec[ievec].p)**4) for ievec in range(N)),dtype=float))
			# ~ lambdainf[ialpha,iav]=N*np.sum(np.fromiter((np.sum(np.abs(fo.eigenvec[ievec].x[i0])**4) for ievec in range(N)),dtype=float))
			
			# ~ for ievec in range(N):
				# ~ ipr[ialpha,iav]+=np.sum(np.abs(fo.eigenvec[ievec].p)**4)
				# ~ lambdainf[ialpha,iav]+=N*np.sum(np.abs(fo.eigenvec[ievec].x[i0])**4)
	
	np.savez(wdir+"dataruns/"+str(runid),lambdainf=N*lambdainf,ipr=ipr)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	nav=int(params['nav'])
	nruns=int(params['nruns'])	
	
	alphat=np.array([0.049,0.191,0.322,0.446,0.564,0.686,0.861])
	nalpha=alphat.shape[0]

	lambdainf=np.zeros((nalpha,int(nruns*nav/nalpha)))
	ipr=np.zeros((nalpha,int(nruns*nav/nalpha)))
	lambdainf_mean=np.zeros(nalpha)
	ipr_mean=np.zeros(nalpha)
	lambdainf_std=np.zeros(nalpha)
	ipr_std=np.zeros(nalpha)
	
	print(nav,nruns)
	
	for i in range(nruns):
		ialpha=i%nalpha
		data=np.load(wdir+"dataruns/"+str(i)+".npz")
		lambdainf[ialpha,int(i/nalpha)*nav:(int(i/nalpha)+1)*nav]=data['lambdainf']
		ipr[ialpha,int(i/nalpha)*nav:(int(i/nalpha)+1)*nav]=data['ipr']
		data.close()

	lambdainf=lambdainf-1
	lambdainf_mean=np.mean(lambdainf,axis=1)
	lambdainf_std=np.std(lambdainf,axis=1)
	
	ipr_mean=np.mean(ipr,axis=1)
	ipr_std=np.std(ipr,axis=1)
	
	print(lambdainf_mean,lambdainf_std)
	
	np.savez(wdir+"data",
		lambdainf_mean=lambdainf_mean,
		lambdainf_std=lambdainf_std,
		ipr_mean=ipr_mean,
		ipr_std=ipr_std,
		alpha=alphat)
	os.system("rm -r "+wdir+"dataruns/")

if mode=="plot":
	params=readInput(wdir+"params.txt")
	N=int(params['N'])
	
	data=np.load(wdir+"data.npz")
	lambdainf=data['lambdainf']
	alpha=data['alpha']
	data.close()
	
	ax=plt.gca()

	ax.plot(alpha,lambdainf[:,3])
	#print(np.mean(lambdainf))

	plt.savefig(wdir+"forme.png", bbox_inches = 'tight',format="png")

				
	


