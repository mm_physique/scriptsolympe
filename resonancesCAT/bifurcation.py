import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

mode=sys.argv[1]
wdir=sys.argv[2]

########################################################################
# Pour comparer la dynamique TB/exact sans potentiel extérieur
# Calcule pour trois valeurs de N différentes, pour voir des effets 
# de bords.
# Calcule pour chaotic et régulier.
########################################################################


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	e=float(params['epsilon'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	nx0=int(params['nx0'])
	nh=int(params['nh'])
	dtmax=int(params['dtmax'])
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	gmin=float(params['gmin'])
	gmax=float(params['gmax'])
	
	
	gamma=np.linspace(gmin,gmax,nruns)[runid]
	
	x0t=np.linspace(-np.pi,np.pi,nx0)
	xstd=np.zeros(nx0)
	h=hmin
		
	for h in np.linspace(hmin,hmax,nh):
	#for dtmax in np.arange(15,20):
	
		grid=Grid(Npcell,h,xmax=4*np.pi)
		pot=ModulatedPendulum(e,gamma)
		
		
		fo=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)

		for i,x0 in enumerate(x0t): 
			wf=WaveFunction(grid)
			wf.setState("coherent")
			wf.shift("x",x0)
			
			for it in range(dtmax): 
				wf=fo%wf
			
			#xm=np.sum(np.abs(grid.x)*np.abs(wf.x)**2)
			
			wf0=WaveFunction(grid)
			wf0.setState("coherent")
			wf0.shift("x",x0)
			xstd[i]+= wf//wf0
	xstd/=np.max(xstd)
	
	np.savez(wdir+"dataruns/"+str(runid),"w", xstd=xstd)		
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	e=float(params['epsilon'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	nx0=int(params['nx0'])
	nh=int(params['nh'])
	dtmax=int(params['dtmax'])
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	gmin=float(params['gmin'])
	gmax=float(params['gmax'])
	
	
	gamma=np.linspace(gmin,gmax,nruns)
	xstd=np.zeros((nruns,nx0))
	x0t=np.linspace(-np.pi,np.pi,nx0)
	
	for irun in range(nruns):
		data=np.load(wdir+"dataruns/"+str(irun)+".npz")
		xstd[irun]=data['xstd']

	np.savez(wdir+"data","w", gamma=gamma,xstd=xstd,x=x0t)	
	
	os.system("rm -r "+wdir+"dataruns/")
	
	
if mode=="plot":
	

	data=np.load(wdir+"data.npz")
	gamma=data['gamma']
	x=data['x']
	xstd=data['xstd']
	data.close()
	
	xstd=np.swapaxes(xstd,0,1)
	
	ax=plt.gca()
	X,Y=np.meshgrid(gamma,x)
	
	ax.contourf(X, Y, xstd, 150, cmap=plt.cm.gnuplot)
	


	plt.savefig(wdir+"xstd.png", bbox_inches='tight',dpi=250)	
	
