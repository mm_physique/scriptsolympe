import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *

from matplotlib import cm

mode=sys.argv[1]
wdir=sys.argv[2]


if mode=="initialize":
	os.mkdir(wdir)
	os.mkdir(wdir+"dataruns")
	
	inputfile=sys.argv[3]+".txt"
	os.system("mv "+inputfile+" "+wdir+"params.txt")
	nruns=int(sys.argv[4])
	addParams(wdir+"params.txt",{'nruns':nruns})
	
if mode=="compute":
	runid=int(sys.argv[3])-1
	
	params=readInput(wdir+"params.txt")
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	x0=float(params['x0'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	
	
	h=np.linspace(hmin,hmax,nruns,endpoint=False)[runid]

	grid=Grid(Npcell,h)
	pot=ModulatedPendulum(e,gamma)
	floquet=FloquetTimePropagator(grid,pot,T0=4*np.pi,idtmax=1000)

	wf=WaveFunction(grid)
	wf.setState("coherent",xpratio=2.0)
	wf.shift("x",x0)
	
	floquet.diagonalize()
	
	overlaps=floquet.orderEigenstatesWithOverlapOn(wf)
	quasienergies=floquet.quasienergy
	symX=np.array([floquet.eigenvec[i].isSym("x") for i in range(Npcell)])
	
	np.savez(wdir+"dataruns/"+str(runid),"w", quasienergies=quasienergies, overlaps=overlaps,symX=symX)
	
if mode=="gather":
	params=readInput(wdir+"params.txt")
	gamma=float(params['gamma'])
	e=float(params['epsilon'])
	x0=float(params['x0'])
	Npcell=int(params['Npcell'])
	nruns=int(params['nruns'])
	
	hmin=float(params['hmin'])
	hmax=float(params['hmax'])
	
	ht=np.linspace(hmin,hmax,nruns,endpoint=False)
	
	quasienergies=np.zeros((nruns,Npcell))
	h=np.zeros((nruns,Npcell))
	overlaps=np.zeros((nruns,Npcell),dtype=complex)
	symX=np.zeros((nruns,Npcell))
	
	
	for irun in range(nruns):
		data=np.load(wdir+"dataruns/"+str(irun)+".npz")
		quasienergies[irun]=data['quasienergies']
		overlaps[irun]=data['overlaps']
		symX[irun]=data['symX']
		h[irun]=ht[irun]*np.ones(Npcell)
		data.close()

	np.savez(wdir+"data","w", quasienergies=quasienergies, overlaps=overlaps,h=h,symX=symX)	
	
	os.system("rm -r "+wdir+"dataruns/")
	
if mode=="plot":
	
	data=np.load(wdir+"data.npz")
	quasienergies=np.fliplr(data['quasienergies'])
	overlaps=np.fliplr(data['overlaps'])
	symX=np.fliplr(data['symX'])
	h=np.fliplr(data['h'])
	data.close()
	
	ax=plt.gca()
	ax.set_xlabel(r"$\beta$")
	ax.set_ylabel(r"$qE/h$")
	# ~ ax.set_xlim(np.min(beta),np.max(beta))
	
	indSym=np.nonzero(symX)
	
	
	
	indAsym=np.nonzero(np.logical_not(symX))
	hmsym=1/h[indSym]
	qEsym=quasienergies[indSym]/h[indSym]
	
	r=np.abs(overlaps[indSym])**2/np.max(np.abs(overlaps[indSym])**2)
	cSym=cm.Blues(r)
	cSym[:,3]=r*(r>0.5)+(r/0.5)**0.5*(r<0.5)
	
	
	r=np.abs(overlaps[indAsym])**2/np.max(np.abs(overlaps[indAsym])**2)
	cAsym=cm.RdYlGn(r)
	cAsym[:,3]=r*(r>0.5)+(r/0.5)**0.5*(r<0.5)
	

	plt.scatter(1/h[indAsym],quasienergies[indAsym]/h[indAsym],c=cAsym,s=1**2)
	plt.scatter(1/h[indSym],quasienergies[indSym]/h[indSym],c=cSym,s=1**2)
	

	plt.savefig(wdir+"spectrum.png", bbox_inches='tight',dpi=250)
	
	
	
